export default {
  async fetch(request, env, ctx) {
    return await handleRequest(request, env, ctx);
  },
};

async function handleRequest(request, env, ctx) {
  // handles sending a mail
  // vasic request puts it in storage 
  // then alarm sends any storage available
  // so request must add email to storage and set alarm
  // on alarm all stored emails are retrieved, sent and  deleted 
  // for testing we may need to stub out mail call or see if they have a test option
  if (request.headers.get('Access-Key') !== env.ACCESS_KEY) {
    return new Response('Forbidden',{ status: 403 })
  }
  const namespace = request.headers.get('idFromName');
  if (!namespace) return new Response('Version must be supplied in a request header called idFromName ', { status: 400 })
  const id = env.DOMAIL.idFromName(namespace);
  let obj = env.DOMAIL.get(id);
  const data = await request.json();
  data.from = namespace
  ctx.waitUntil(
    obj.fetch(request.url,{
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json' 
      }
    })
  )
  return new Response('Mail queued');
}

export class DOmail {

  constructor(state, env) {
    this.state = state;
    this.state.blockConcurrencyWhile(async () => {

    });
  }

  emailToEmailObj(emailAddr) {
    const regex = /<(.+@.+)>/g;
    const match = emailAddr.match(regex);
    if (match) {
      const email = match[0].slice(1, -1);
      const name = emailAddr.replace(regex, "").trim();
      console.log("email is", email, name);
      return { email, name };
    } else {
      return { email: emailAddr };
    }
  }

  async sendMail({ to, subject, body, from }) {
    to = Array.isArray(to) ? to : [to];
    if (!from)
      from = "server@geocam.xyz";
    if (!body)
      body = "";
    if (typeof body === "string")
      body = { type: "text/plain", value: body };
    const content = Array.isArray(body) ? body : [body];
    let send_request = new Request("https://api.mailchannels.net/tx/v1/send", {
      method: "POST",
      headers: {
        "content-type": "application/json"
      },
      body: JSON.stringify({
        personalizations: [
          {
            to: to.map((t) => this.emailToEmailObj(t))
          }
        ],
        from: this.emailToEmailObj(from),
        subject,
        content
      })
    });
    const resp = await fetch(send_request);
    console.log('send mail resp', resp)
    if (resp.ok) {
      return new Response(JSON.stringify({ to, subject, sent: true }), {
        headers: {
          "content-type": "application/json;charset=UTF-8"
        }
      });
    } else {
      return resp;
    }
  }

  async alarm() {
    console.log('handlimg alarm')
    const stored = await this.state.storage.list();
    for (const [key, value] of stored) {
      this.sendMail(value).then(() => {
        this.state.storage.delete(key);
      })
    }
  }

  async fetch(request) {
    request.json()
    .then(data => {
      const key = new Date().getTime();
      return this.state.storage.put(key, data, { allowUnconfirmed: true });
    })
    .then(() => {
      console.log('setting alarm')
       this.state.storage.setAlarm(Date.now());
      // this.alarm();
    })
    return new Response('hello domail')
  }

}