import { test, expect } from "@playwright/test";

let sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

let publicPem;

test.beforeAll(async ({ request }) => {
  // Create a new repository
  let started;
  let count = 0
  do {
    await sleep(2000);
    try {
      count += 1;
      const response = await request.get(`/1/`);
      publicPem = await response.text();
      started = true;
    } catch (err) {
      started = false;
      if (count > 10) {
        throw (err)
      }
    }
  } while (!started);
  expect(started).toBeTruthy();
});

test("Request without a version fails", async ({ request }) => {
  const response = await request.get(`/`);
  expect(response.ok()).toEqual(false);
});
